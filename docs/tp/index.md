L'objectif des exercices est d'appréhender Ansible par la mise en pratique d'une installation d'un environnement LAMP (Linux Apache MySQL PHP) simple qui hébergera une instance du [CMS Wordpress](https://fr.wordpress.org/).

Nous étudierons ainsi les différents concepts Ansible d'inventaire, de playbook, de role, de task, de module, ... dans le cadre d'une déploiement.
!!! notice
    Ansible s'utilise également dans d'autres cas d'usage comme la gestion de workflow par exemple.
