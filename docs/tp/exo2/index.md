# Rappel environnement

Pour les besoins des TPs, nous mettons à disposition de chaque participant:

- une workstation commune servant de `control node`
- 2 serveurs Linux dédiés pour les `managed node`

<!-- Chaque participant dispose d'un numéro XX (de 01 à 20) attribué en début de séance permettant d'identifier ces ressources:

- identifiant SSH: `formation-XX`
- serveurs:
	- `sydre-db-XX`
	- `sydre-web-XX`

Pour vous connecter à la plate-forme de TP, il vous suffit d'ouvrir une session SSH vers la workstation:

`$ ssh -l formation-XX sydre-workstation`  (pass: `sydre-2020`) -->

# Objectifs

1. Utiliser les rôles Galaxy
2. Créer son 1er rôle
3. Installer une plateforme LAMP
4. Préparer l'installation de WordPress

# Les rôles Ansible

Pour mémoire, un rôle permet de créer des tâches facilement réutilisables dans des playbooks. Ils sont composés d'un ensemble de fichiers YAML structurés dans une arborescence normalisée:

```
├── README.md		# description du role
├── defaults		# variables par défaut du rôle
│   └── main.yml
├── files		    # contient des fichiers à déployer
├── handlers		# actions déclenchées par une notification
│   └── main.yml
├── meta		    # metadonnées et notamment les dépendances
│   └── main.yml
├── tasks		    # contient la liste des tâches à exécuter
│   └── main.yml
├── templates		# contient des templates au format Jinja2
|   └── template.j2
├── tests
│   ├── inventory
│   └── test.yml
└── vars		    # autres variables pour le rôle
    └── main.yml
```

Ansible mets à la disposition de la communauté un hub qui permet de publier et diffuser des rôles: https://galaxy.ansible.com

Les rôles publiés peuvent être utilisés directement ou servir d'inspiration pour réaliser vos premiers rôles.

On appelle les rôles à partir d'un play contenu dans un playbook:

```yaml
---
- hosts: webservers
  roles:
    - role_name1
    - role: role_name2
      vars:
        role_var1: value
        role_var2:
          - list_item1
          - list_item2
```

# Utiliser un rôle existant

Ansible galaxy (https://galaxy.ansible.com/) propose un moteur de recherche pour trouver le rôle souhaité:

![](https://i.imgur.com/FYI1fQ5.png)

Dans cet exemple, nous recherchons un rôle pour installer `docker` avec un filtre sur les plateformes de type Debian. Le score et le nombre de téléchargements peuvent être une bonne indication sur la popularité du rôle.

![](https://i.imgur.com/cAXvHHv.png)

On constate qu'un rôle correspond finalement à un dépôt GitHub. Son identifiant est de type `[login_github].[role_name]`

Pour installer un rôle:
_(ansible 2.7 et inférieur):_
```bash
$ ansible-galaxy install -p /path/to/role [role_name]
```

_(sous Ansible 2.8+):_
```bash
$ ansible-galaxy role install -p /path/to/role [role_name]
```

Il convient de lire la documentation (usuellement sur le dépôt GitHub) du rôle car il peut y avoir des informations sur l'utilisation de variables internes au rôle pour adapter le comportement de celui-ci (exemple: spécifier la version de `php` à installer).

# Créer un rôle

Pour générer l'arborescence normalisée d'un rôle et facilité ainsi une éventuelle publication sur le HUB Galaxy, vous pouvez utiliser la commande suivante.

_(ansible 2.7 et inférieur):_
```bash
$ ansible-galaxy init --init-path /path/to/roles [login_github].[role_name]
```

_(sous Ansible 2.8+):_
```bash
$ ansible-galaxy role init --init-path /path/to/roles [login_github].[role_name]
```



# Mise en pratique

## Lister les tâches à réaliser

Nous souhaitons créer un rôle `sydre.apache` pour installer Apache HTTPD avec l'utilisation du module PHP:

- installer les paquets nécéssaires:
	- module: `package`
	- fichier à compléter: `./roles/sydre.apache/tasks/main.yml`
	- Liste des packages:
		- apache2
		- apache2-utils
		- php
		- php-curl
        - php-gd
        - php-intl
        - php-json
        - php-mbstring
        - php-mysql
        - php-xml
		- php-zip
		- python-pymysql (utilisé uniquement pour le rôle mariadb)

- s'assurer que le service `apache2` est activé et démarré
	- module: `service`
	- fichier à compléter: `./roles/sydre.apache/tasks/main.yml`

- copier une page de test (fonction `phpinfo()` de php):
	- module: `copy`
	- fichier à créer dans: `./roles/sydre.apache/files/`
	- contenu du fichier: `<?php phpinfo(); ?>`
	- destination: `/var/www/html`
	- nom du fichier: `test.php`

- Suppression du fichier index.html par défaut
	- module: `file`
		- fichier à supprimer: `/var/www/html/index.html`

- sécuriser la configuration apache (`security.conf`)
	- module: `lineinfile`
	- fichier à compléter: `./roles/sydre.apache/tasks/main.yml`
	- fichier à modifier: `/etc/apache2/conf-available/security.conf`
	- ligne à modifier: `ServerSignature On` en `ServerSignature Off`

	- utiliser le mécanisme notify / handler pour recharger la configuration apache
		- notify:
		- handler: `./roles/sydre.apache/handlers`

!!! info
    Les handlers sont des actions déclenchées sur la notification d'un changement typiquement comme un restart de service après modification d'un fichier de configuration: https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html#handlers-running-operations-on-change


## Implémentation du rôle `sydre.apache`

- Créer un répertoire `roles` pour accueillir nos rôles (à la racine du répertoire `ansible` de votre homedir sur `sydre-workstation`):
```bash
$ mkdir -p ~/ansible/roles
```

- Initialiser la structure du rôle:
_(ansible 2.7 et inférieur):_
```bash
$ ansible-galaxy init --init-path ~/ansible/roles sydre.apache
```
_(ansible 2.8+):_
```bash
$ ansible-galaxy role init --init-path ~/ansible/roles sydre.apache
```

- compléter les différents fichiers en vous aidant de la documentation de chaque module

- créer un playbook `infra.yml` contenant un play utilisant le rôle `sydre.apache` pour le groupe `webservers`

- exécuter le playbook `infra.yml`:
```bash
$ ansible-playbook infra.yml
```

- tester le fonctionnement de l'environnement Apache/PHP en consultant l'URL: [http://sydre-web-XX/test.php](http://sydre-web-XX/test.php)


## Importer un rôle `mariadb`

- Importer le rôle `diodonfrost.mariadb` depuis Ansible Galaxy
_(ansible 2.7 et inférieur):_
```bash
$ ansible-galaxy install -p ~/ansible/roles diodonfrost.mariadb
```
_(ansible 2.8+):_
```bash
$ ansible-galaxy role install -p ~/ansible/roles diodonfrost.mariadb
```
- Ajouter le play correspondant dans le playbook `infra.yml` pour le groupe `dbservers`

- Définir les variables au niveau du play :
	  - mariadb_bind_address: `'0.0.0.0'`
	  - mariadb_database : `wordpress_db`
	  - mariadb_user : `wordpress_user`
  		- password : `wordpress_password`
		- privilège : `ALL` sur la database `wordpress_db` depuis **%** (voir: https://docs.ansible.com/ansible/latest/modules/mysql_user_module.html)

tel que défini dans le rôle `diodonfrost.mariadb` afin de préparer l'installation Wordpress qui va suivre (voir la documentation du rôle sur [Github](https://github.com/diodonfrost/ansible-role-mariadb))

- en SSH sur votre serveur `sydre-db-XX`, tester le bon fonctionnement du rôle avec la commande:
```bash
$ mysql -u wordpress_user -h 127.0.0.1 -p wordpress_db
```
