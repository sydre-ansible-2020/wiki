# Rôle `sydre.apache`

```bash
git clone --depth 1 https://gitlab.com/sydre-ansible-2020/ansible-role-sydre-apache.git ~/ansible/roles/sydre.apache
```

# Playbook `infra.yml`

```yaml
---
- name: Install Apache PHP
  hosts: webservers
  become: true
  roles:
    - sydre.apache

- name: Install MariaDB
  hosts: dbservers
  become: true

  vars:
    mariadb_bind_address: '0.0.0.0'

    mariadb_database:
      - name: wordpress_db
        state: present

    mariadb_user:
      - name: wordpress_user
        host: '%'
        password: "wordpress_password"
        priv: 'wordpress_db.*:ALL'
        encrypted: false

  pre_tasks:
    - name: Install dependencies
      package:
        name: python-pymysql
        state: present

  roles:
    - diodonfrost.mariadb
```
