# Description sommaire de la plateforme

Afin de réaliser les exercices, une plateforme à base de machines virtuelles est mis à disposition.

La plateforme est prévue pour 20 participants dont les comptes sont de la forme `formation-XX` où XX est un entier formaté sur 2 caractères, entre 01 et 20.

Le mot de passe de ces comptes est unique : `sydre-2020`.

Chaque participant se voit également affecter 2 machines virtuelles nommées `sydre-web-XX` et `sydre-db-XX`. Ce sont les machines virtuelles qui hébergeront les services permettant de faire tourner votre Wordpress.


Par un soucis de résilience, une plateforme Université de Rouen et une plateforme identique CRIANN ont été préparé. La plateforme cible à utiliser vous sera communiquée en début de séance.

## Les machines
La salle informatique est dotée de plusieurs postes de travail dont l'authentification est spécifique.
Les éléments d'authentification pour les postes vous seront fournis dès votre arrivée.

La connexion initiale à la plateforme se fait avec votre compte `formation-XX` sur `sydre-workstation` (nommée par la suite `workstation` de manière interchangeable), une passerelle de connexion SSH sur laquelle l'environnement Ansible est installé et qui vous servira d'espace de travail. C'est via cette passerelle que vous réaliserez les actions Ansible pour monter le Wordpress.

Dans un souci de gain de temps, Ansible est déjà installé sur la `workstation` directement depuis le gestionnaire de paquet de la distribution.

## Gestion des machines virtuelles

En cas de soucis avec vos machines virtuelles `sydre-web-XX` et `sydre-db-XX`, vous aurez la possibilité de restaurer les VMs à partir de snapshot de celles-ci.
