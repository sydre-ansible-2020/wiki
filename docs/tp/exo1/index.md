# Objectifs
1. créer un fichier d'inventaire
2. valider le fonctionnement en mode Ad-Hoc
3. récuperer une variable avec les `facts`
4. créer votre premier playbook

# Création de l'inventaire

- dans votre `homedir`, créer un répertoire `ansible`
- passer dans le repertoire `ansible`
- copier le fichier de configuration `/etc/ansible/ansible.cfg` dans le répertoire de travail (`~/ansible/`)
- modifier la ligne `#inventory = /etc/ansible/hosts` en `inventory = hosts` du fichier `~/ansible/ansible.cfg`

!!! info
    Cette opération nous évite de spécifier le path (avec l'option `-i /path/to/inventory`) du fichier d'inventaire pour les commandes `ansible` et `ansible-playbook`

- créer un fichier `hosts` (vim, nano) au format `INI`, avec les sections (désigne un groupe de machines) `webservers`et `dbservers`:

```toml
[webservers]
sydre-web-XX

[dbservers]
sydre-db-XX
```
- pour plus d'information sur la structure du fichier d'inventaire:
[https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)

# Utiliser les commandes Ad-Hoc

-  introduction au commande Ad-Hoc:
[https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html)

- Structure d'une commande Ad-Hoc:
`$ ansible [pattern] -m [module] -a [arguments]`

!!! info
    `pattern`: peut être une machine, un groupe, ou `all` pour désigner l'ensemble des machines


## tester votre infra avec le module `ping`

[https://docs.ansible.com/ansible/latest/modules/ping_module.html](https://docs.ansible.com/ansible/latest/modules/ping_module.html)

```json
$ ansible all -m ping
sydre-db-01 | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
sydre-web-01 | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```

## Afficher le contenu du fichier `/etc/issue` sur les `managed nodes` avec le module `shell`

!!! info
    Vous pouvez utiliser la commande `ansible-doc` pour obtenir des infos sur un module: `$ ansible-doc shell`


Pour cette commande, nous allons spécifier une exécution uniquement sur les machines du groupe `webservers`

```bash
$ ansible webservers -m shell -a 'cat /etc/issue'
sydre-web-01 | CHANGED | rc=0 >>
Debian GNU/Linux 10 \n \l
```

## Récupérer une variable `facts` avec le module `setup`

!!! success
    Ansible utilise les "Facts" pour collecter des informations sur les managed nodes. Ce mecanisme permet de peupler des variables (ex: hostname, @IP, OS Version..) `ansible_xx` utilisables dans les playbooks et des templates JinJa2
    ```
    $ ansible dbservers  -m setup
    $ ansible dbservers  -m setup -a "filter=ansible_distribution*"
    sydre-db-01 | SUCCESS => {
        "ansible_facts": {
            "ansible_distribution": "Debian",
            "ansible_distribution_file_parsed": true,
            "ansible_distribution_file_path": "/etc/os-release",
            "ansible_distribution_file_variety": "Debian",
            "ansible_distribution_major_version": "10",
            "ansible_distribution_release": "buster",
            "ansible_distribution_version": "10.2"
        },
        "changed": false
    }
    ```

## Installer le paquet `htop` sur les `managed nodes`

```
$ ansible all -m package -a 'name=htop state=latest' -b

pour tester:

$ ssh sydre-web-XX
$ htop
$ exit
```

Dans cette exemple, le module `package` appelle la commande `apt install` qui  nécéssite une élévation de privilèges obtenue avec l'option `-b` ou `--become` :
[https://docs.ansible.com/ansible/latest/user_guide/become.html](https://docs.ansible.com/ansible/latest/user_guide/become.html)

# Utiliser un playbook

Dans l'exercice précédent, nous avons utilisé le module `package` avec 2 arguments: `name=htop` et `state=latest`. Nous pouvons facilement transformer cette commande Ad-Hoc en playbook ecrit en YAML.

[https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html](https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html)
[https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html)

`$ vim install_nano.yml`

```yaml
---
- hosts: webservers
  become: yes
  tasks:
  - name: install package nano
    package:
      name: nano
      state: present
```
En version Ad-Hoc:
`$ ansible webservers -m package -a 'name=nano state=present' -b`

Ce playbook contient un seul `play` (ensemble de tâches ayant une même cible d'hôtes) constitué d'une `tasks` qui appelle un `module` (package) avec 2 arguments.


On teste la syntaxe YAML du playbook
```bash
ansible-playbook --syntax-check install_nano.yml
```
Si aucun message d'erreur ne s'affiche, la syntaxe est correcte.

Vous pouvez également simuler (aucun changement ne sera apporté) le lancement du playbook:
```bash
ansible-playbook --check install_nano.yml
```


Pour l'exécuter, on utilise la commande `ansible-playbook`:

```toml
$ ansible-playbook install_nano.yml

PLAY [webservers]
******************************************************
******************************************************

TASK [Gathering Facts]
******************************************************
******************************************************
ok: [sydre-web-01]

TASK [install package nano]
******************************************************
******************************************************
ok: [sydre-web-01]

PLAY RECAP
******************************************************
******************************************************
sydre-web-01    : ok=2 changed=0  unreachable=0    failed=0

```
