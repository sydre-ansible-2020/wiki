# Exercice cas pratique

Il vous est donné la tâche de construire une architecture N-tier pour l'hébergement résilient d'un site web Wordpress, avec :

  - 2 reverse-proxies qui répartiront la charge et seront les terminaisons SSL pour présenter le site web,
  - 10 serveurs web partageant un espace de stockage fichier (avec lock POSIX),
  - 1 serveur MySQL Master afin d'héberger les données SQL de votre site web institutionnel sous Wordpress,
  - 1 serveur MySQL Slave afin d'assurer la résilience.

L'ensemble sous Linux et pour une simplicité de l'exercice, ne seront pas considérés le stockage partagé et le mécanisme de résilience MySQL.

## Spécifications
Il vous est demandé:

  - de prévoir le déploiement sur une infrastructure qui peut-être "on-premise" (sur vos machines) ou répartie chez des hébergeurs en offre de type IaaS  (Infrastructure as a Service)
  - de prévoir l'horizontal-scaling (l'agrandissement en nombre d'une catégorie de serveurs) des ressources pour pouvoir monter en charge
  - de prévoir une protection minimaliste des scans web et DDoS HTTP
  - de prévoir les maintenances programmées des différentes machines sans impact visible pour les usagers (rolling upgrade, ...)
  - de prévoir la migration rapide d'un hébergeur à un autre

## Brainstorming

Réfléchissons ensemble à ce qu'implique ces spécifications :

  - il y a 3 groupes de machines :
    - les reverse-proxies qui réalisent une fonction de Load Balancers en répartissant les connexions HTTP vers l'ensemble des serveurs web
    - les serveurs web qui présenteront le site web aux Load Balancers et qui hébergeront l'application web
    - le serveur de base de données qui permettra à l'application web d'être dynamique (stockage des articles, post, rédacteurs, commentaires, ...)
  - le nombre de machines des différentes catégories devraient être suffisantes pour réaliser l'objectif
  - la liste des fonctions peut être résumée comme ceci :
    - `haproxy` pour la partie Load Balancer
    - `apache` + `php` pour la partie serveur web + `redis` pour le partage des sessions PHP entre les machines
    - `mariadb` + `galera` pour la partie base de données MySQL
    - `fail2ban` pour une protection scan web/DDoS HTTP
  - les machines doivent pouvoir être configurées pour un environnement d'hébergement souple (différents serveurs resolveurs DNS, NTP, relai SMTP d'un hébergeur à un autre)

## Répartition des usages

| rôle \\ groupe    | webservers | reverseproxies | dbservers |
|:----------------- |:----------:|:--------------:|:---------:|
| `base_linux`      |     X      |       X        |     X     |
| `apache`          |     X      |                |           |
| `php`             |     X      |                |           |
| `mariadb`         |            |                |     X     |
| `galera`          |            |                |     X     |
| `redis`           |     X      |                |           |
| `haproxy`         |            |       X        |           |
| `fail2ban`        |            |       X        |           |

où `base_linux` est un rôle Ansible pour la configuration de base d'un serveur Linux (configuration des clients NTP, SMTP et DNS, installation des outils de gestion, ...).

## Les workflows à prévoir
Nous distinguons ici différents workflows :

  - le déploiement de la solution se réalisera par étape (et probablement dans cette ordre) :
    - pour tous, l'installation des outils de gestion et la configuration OS de la machine
    - pour les dbservers, l'installation de `mariadb` et `galera`
    - pour les webservers, l'installation des logiciels `apache`, `php` et `redis`
    - sur l'un des webservers, le déploiement de Wordpress sur l'espace partagé
    - pour les reverse-proxies, `haproxy` et `fail2ban`
  - la gestion au quotidien nécessitera
    - de pouvoir maintenir à jour les OS en désactivant, à tour de rôle, les serveurs puis en les réintégrant
    - de pouvoir étendre la protection scan web/DDoS HTTP avec de nouvelles règles filtrantes
    - ...

Ces différents workflows peuvent être adressés par différents playbooks Ansible.
