# Installation d'Ansible sur un environnement Linux


Il existe plusieurs façon d'installer Ansible sur une distribution Linux.

- par dépôt de l'OS
- par le gestionnaire de dépendance python : pip
- sous Ubuntu, il existe un dépôt PPA maintenu par la communauté

Les différentes méthodes sont décrites sur le site officiel d'Ansible : [https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

L'installation d'Ansible sur le plateforme est réalisée avec le dépôt PPA car cela permet d'avoir accès aux dernières versions d'Ansible
