**Architecture Ansible**
-------
![](https://i.imgur.com/4fYNMiF.png)

# Terminologie

  * **control node** : machine sur laquelle Ansible est installé et exécuté
  * **managed nodes** : machines sur lesquelles les tâches décrites par les playbooks sont exécutées
  * **playbook** : fichier YAML qui contient une liste de **play**
  * **play** : un ensemble de tâches (issues des **role**, **block** ou tasks individuelles) à réaliser sur un jeu d'hôtes sous différentes modalités de connexion
  * **role** : regroupement réutilisable de tâches (exemple : un rôle `apache` permettant l'installation de base d'un service Apache HTTPD)
  * **tasks** : tâche qui utilise un **module** inclut dans Ansible
  * **inventory** : fichier inventaire statique ou dynamique (script externe) déclarant les hôtes et leurs appartenances à des groupes ainsi que des variables associées (au groupe et/ou à l'hôte)
  * **facts** : variable collectée de l'hôte pendant que le playbook est exécuté (uniquement si `gather_facts` n'est pas désactivé)
  * **plugins** : ensemble de scripts Python pouvant servir à réaliser des tests ( type `test`), filtrer des éléments ( type `filter`), injecter de variables ( type `vars`), ...
  * **lookup** : plugin permettant d'étendre l'accès aux données (exemple : le lookup `file` permet de récupérer le contenu d'un fichier sur le control node)
  * **loop** : boucle dans l'exécution d'une tâche ou d'un bloc de tâches au sein d'un playbook (limitation par rapport aux **block**)
  * **block** : regroupement de tâches consécutives ayant des caractéristiques d'exécution identiques (exemple: délégué au **control node**)
  * **become** : ensemble de plugins spécifiant que l'exécution de la tâche (ou du playbook) nécessitera une escalade de privilèges (exemple: **sudo** sous Linux, contexte **enable** sur les équipements réseaux, ...)
  * **templates** : fichier patron utilisant la syntaxe Jinja2 (moteur de template pour Python)

# Les principales commandes

  * **ansible** : permet l'exécution d'une tâche en utilisant un module Ansible, sur un ou plusieurs hôtes référencés
  * **ansible-doc** : accès à la documentation sur les modules et plugins d'Ansible
  * **ansible-playbook** : permet l'exécution des playbooks
  * **ansible-config** : permet de lister la configuration actuelle ou l'ensemble des paramètres de configuration
  * **ansible-inventory** : permet de lister les hôtes et groupes dont ils sont membres. Avec l'option `--vars` il est même possible d'avoir les variables associées à chaque hôte.
  * **ansible-vault** : permet le chiffrement et déchiffrement de fichiers YAML pour un stockage sécurisé d'informations secrètes pouvant être utilisées dans un playbook
  * **ansible-galaxy** : permet de gérer les rôles et collections de rôles issues de Ansible Galaxy

# Principe de fonctionnement

Ansible est une solution développée en Python qui permet l'exécution un ensemble de tâches ("tasks") sur un ensemble de machines (par défaut distantes mais peut aussi être en local).
L'exécution est réalisée au travers de plugins de connexion (par défaut le client SSH du control node) et suivant un ordre d'exécution décrit par le playbook.   L'exécution est prévu pour être idempotent (de multiples exécutions sont censés produire le même état).
Le playbook décrit un ensemble de tâches utilisant des modules Ansible réalisant des actions unitaires sur chaque hôte spécifié.

Lors de l'exécution d'un playbook, Ansible réalise plusieurs actions :

  * lire son fichier de configuration :
    * où se trouve le fichier inventaire,
    * quelles sont les valeurs par défaut pour les connexions,
    * ...
  * évaluer les hôtes sur lesquels le playbook va devoir s'exécuter :
    * résolution groupes en hôtes
  * évaluer les facts qui s'appliquent hôte par hôte (héritage des variables et, sauf indication contraire, lecture de l'état de la cible)
  * exécuter, par défaut en parallèle, les tâches du playbook dans l'ordre en évaluant si certaines ne sont pas à réaliser (différentes raisons : état déjà final, conditions non satisfaites, ...)

# Arborescence de fichiers

voir les [Best practices](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html)

# Les rôles
Le rôle doit être pensé réutilisable avec des possibilités de surcharge des variables qui conditionnent son usage.

  * un rôle `base_linux` se voudra donc utilisable sur tous les serveurs linux (avec détection de l'OS pour adapter le comportement)
  * un rôle `apache` permettra d'installer Apache HTTPD depuis les dépôts de l'OS en fonction de l'OS éventuellement en définissant une structure de dossier différente de ce que l'OS installe.
  * un rôle `ntp_client` possédera une variable `ntp_servers` qui est une liste de serveurs NTP. Ainsi il sera possible de surcharger cette liste avec une autre lors de l'appel à ce rôle pour une utilisation dans un contexte différent (les serveurs internes NTP de l'Université de Rouen ne sont pas les serveurs NTP du CRIANN et inversement)

Les valeurs par défaut de ces variables sont à définir dans le fichier `defaults/main.yml` du rôle.

!!! tip

    - faire un brainstorming des déploiements à venir afin d'établir une matrice de ce que les machines ont/auront en commun afin de délimiter les périmétres fonctionnels des rôles. Ainsi, chaque rôle sera codé une seule fois, normalement sans avoir besoin d'y revenir et de passer des heures à vérifier si la modification du rôle aura un impact sur les autres rôles et playbooks.

# Les hôtes et groupes

!!! tip
    Regrouper par "Who-What-Where" :

    - regrouper les machines dans des groupes exprimant l'usage et le lieu permet de lancer des tests hors production (environnement de test) avant la mise en production. Vous pourrez ainsi spécifier un inventaire par environnement (test, dev, production)

    - Comme les facts et variables partagent le même espace de nom, il est préférable de préfixer les variables d'un rôle par le nom du rôle pour éviter qu'un rôle affecte le comportement attendu d'un autre rôle ou d'un tâche.
