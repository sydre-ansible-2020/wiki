**SYDRE - formation initiation Ansible**
-------

Formation dispensée dans le cadre du réseau SYDRE Normandie en janvier 2020.

__Formateurs :__

  - Bruno LEVASSEUR, [Université de Rouen Normandie](http://www.univ-rouen.fr) : bruno.levasseur@univ-rouen.fr
  - Sébastien VIGNERON, [CRIANN](https://www.criann.fr) : sebastien.vigneron@criann.fr

# Les ressources
  - [Présentation](https://hackmd.io/@xX60K_TlT26wWX4ySx8KGQ/SkKCcQ-AB#/)
    - **attention**, navigation vers les 4 flèches directionnelles du clavier
  - [Cours - Ansible expliqué](cours/archi.md)
  - [TD - exercice de réflexion](td/index.md)
  - [TP - exercices de déploiement d'un applicatif web](tp/index.md)

  - les dépôts gitlab : https://gitlab.com/sydre-ansible-2020/

# Enquête satisfaction

https://forms.gle/yW2SBFbh7GiGBPy76
